package ru.t1consulting.nkolesnik.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectRemoveByIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.project.ProjectRemoveByIdResponse;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove project by id with tasks by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectRemoveByIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(getToken());
        request.setId(id);
        @NotNull final ProjectRemoveByIdResponse response = getProjectEndpoint().removeProjectById(request);
        @Nullable final ProjectDto project = response.getProject();
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
