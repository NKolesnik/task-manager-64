package ru.t1consulting.nkolesnik.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.util.DateUtil;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Project {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date startDate = new Date();

    @Nullable
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date endDate;

    @NotNull
    private Status status = Status.NOT_STARTED;

    public Project(@NotNull final String name) {
        this.name = name;
    }

}
