package ru.t1consulting.nkolesnik.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;

import java.util.Collection;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    public Collection<Task> getTask() {
        return taskRepository.findAll();
    }

    @NotNull
    public Status[] getStatuses() {
        return Status.values();
    }

    @Nullable
    private Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskRepository.create();
        return "redirect:/tasks";
    }

    @GetMapping("/tasks")
    public ModelAndView list() {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("tasks");
        modelAndView.addObject("tasks", getTask());
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(@NotNull @ModelAttribute("task") final Task task, @NotNull final BindingResult result) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@NotNull @PathVariable("id") final String taskId) {
        @NotNull final Task task = taskRepository.findById(taskId);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", getStatuses());
        modelAndView.addObject("projects", getProjects());
        return modelAndView;
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(@NotNull @PathVariable("id") final String id) {
        taskRepository.removeById(id);
        return "redirect:/tasks";
    }

}
