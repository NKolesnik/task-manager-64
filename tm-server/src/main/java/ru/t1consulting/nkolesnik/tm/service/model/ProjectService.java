package ru.t1consulting.nkolesnik.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.service.model.IProjectService;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.repository.model.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.model.UserRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

    @NotNull
    @Autowired
    protected ProjectRepository repository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public void add(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        repository.saveAndFlush(project);
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if(user == null) throw new UserNotFoundException();
        project.setUser(user);
        repository.saveAndFlush(project);
    }

    @Override
    @Transactional
    public void add(@Nullable final Collection<Project> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        repository.saveAll(projects);
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Collection<Project> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if(user == null) throw new UserNotFoundException();
        for (Project project : projects)
            project.setUser(user);
        repository.saveAll(projects);

    }

    @Override
    @Transactional
    public void set(@Nullable final Collection<Project> projects) {
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        repository.deleteAll();
        repository.saveAll(projects);
    }

    @Override
    @Transactional
    public void set(@Nullable final String userId, @Nullable final Collection<Project> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null || projects.isEmpty()) throw new ProjectNotFoundException();
        repository.deleteAll();
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if(user == null) throw new UserNotFoundException();
        for (Project project : projects)
            project.setUser(user);
        repository.saveAll(projects);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if(user == null) throw new UserNotFoundException();
        project.setUser(user);
        repository.saveAndFlush(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if(user == null) throw new UserNotFoundException();
        project.setUser(user);
        repository.saveAndFlush(project);
        return project;
    }

    @Override
    public long getSize() {
        return repository.count();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @Nullable final List<Project> projects;
        projects = repository.findAll();
        if (projects.isEmpty()) return Collections.emptyList();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Project> projects;
        projects = repository.findAll(userId);
        if (projects.isEmpty()) return Collections.emptyList();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<Project> projectList;
        projectList = repository.findAll(org.springframework.data.domain.Sort.by
                (
                    org.springframework.data.domain.Sort.Direction.DESC,
                    getSortColumnName(comparator)
                )
        );
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<Project> projectList;
        projectList = repository.findAll(org.springframework.data.domain.Sort.by
                (
                        org.springframework.data.domain.Sort.Direction.DESC,
                        getSortColumnName(sort.getComparator())
                )
        );
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<Project> projectList;
        projectList = repository.findAll(userId, getSortColumnName(comparator));
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<Project> projectList;
        projectList = repository.findAll(userId, getSortColumnName(sort.getComparator()));
        if (projectList.isEmpty()) return Collections.emptyList();
        return projectList;
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findById(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @Override
    @Transactional
    public void update(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        repository.saveAndFlush(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = repository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Project project = repository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        repository.delete(project);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeById(userId, project.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeById(userId, id);
    }

}
