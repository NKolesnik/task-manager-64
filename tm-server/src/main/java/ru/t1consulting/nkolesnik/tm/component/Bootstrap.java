package ru.t1consulting.nkolesnik.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.service.ILoggerService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDto;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;
import ru.t1consulting.nkolesnik.tm.endpoint.AbstractEndpoint;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.listener.EntityListener;
import ru.t1consulting.nkolesnik.tm.listener.OperationEventListener;
import ru.t1consulting.nkolesnik.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    AbstractEndpoint[] endpoints;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private Backup backup;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @SneakyThrows
    public void initJMS() {
        BasicConfigurator.configure();
        EntityListener.setConsumer(new OperationEventListener());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initJMS();
        initPID();
        initDemoData();
        initEndpoints();
        loggerService.info("** WELCOME TO TASK-MANAGER SERVER**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
        backup.stop();
    }

    private void initDemoData() {
        @Nullable final UserDto test = userService.findByLogin("test");
        if (test == null) {
            userService.create("test", "test", "test@test.email.ru", Role.USUAL);
        }
        @Nullable final UserDto admin = userService.findByLogin("admin");
        if (admin == null) {
            userService.create("admin", "admin", "admin@admin.email.ru", Role.ADMIN);
        }
        if (admin != null) {
            @NotNull final List<ProjectDto> projects = projectService.findAll(admin.getId());
            if (projects.isEmpty()) {
                projectService.create(admin.getId(), "admin project", "admin project description");
            }
        }
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

}