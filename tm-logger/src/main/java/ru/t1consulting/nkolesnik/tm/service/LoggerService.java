package ru.t1consulting.nkolesnik.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
@NoArgsConstructor
public class LoggerService implements ILoggerService {

    @NotNull
    private static final String HOST = "localhost";

    private static final int PORT = 27017;

    @NotNull
    private static final String DATABASE = "tm";

    @NotNull
    private static final String TABLE_NAME = "table";

    @NotNull
    private final ObjectMapper mapper = new ObjectMapper();

    @NotNull
    private final MongoClient client = new MongoClient(HOST, PORT);

    @NotNull
    private final MongoDatabase database = client.getDatabase(DATABASE);

    @Override
    @SneakyThrows
    public void writeLog(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        @NotNull final Map<String, Object> event = mapper.readValue(message, LinkedHashMap.class);
        @Nullable final String tableName = event.get(TABLE_NAME).toString();
        if (tableName == null || tableName.isEmpty()) return;
        if (database.getCollection(tableName) == null) database.createCollection(tableName);
        @NotNull final MongoCollection<Document> collection = database.getCollection(tableName);
        collection.insertOne(new Document(event));
    }

}
