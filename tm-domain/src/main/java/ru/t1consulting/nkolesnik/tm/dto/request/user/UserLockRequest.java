package ru.t1consulting.nkolesnik.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockRequest(@Nullable String token) {
        super(token);
    }

    public UserLockRequest(@Nullable String token, @Nullable String login) {
        super(token);
        this.login = login;
    }

}

