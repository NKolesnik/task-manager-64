package ru.t1consulting.nkolesnik.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskBindToProjectResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskUnbindFromProjectResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectTaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectTaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String SPACE = "http://endpoint.tm.nkolesnik.t1consulting.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectTaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectTaskEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectTaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    );

    @NotNull
    @WebMethod
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    );

}
